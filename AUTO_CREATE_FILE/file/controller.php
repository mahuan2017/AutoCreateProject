<?php
namespace Home\Controller;
use Think\Controller;
class _NAME_Controller extends Controller {
	public function index(){
		$this->display();
	}

	public function get_list(){
        $where = '1=1';
        $page = I('get.page');
        $pagesize = I('get.pagesize');
        $ret['total'] = M(CONTROLLER_NAME)->where($where)->count();
        $ret['data'] = M(CONTROLLER_NAME)
		->where($where)
        ->page($page,$pagesize)
		->order('id desc')
		->select();

        $ret['isSuccess'] = 1;
        ret($ret);
    
	}

	public function update(){
		$value = I('post.value');
		$id = I('post.id');
		$field = I('post.field');


		$d[$field] = $value;
		M(CONTROLLER_NAME)->where('id = '.$id)->save($d);
		sus();
	}



	public function del(){
		$id = I('post.id');
		M(CONTROLLER_NAME)->where('id = '.$id)->delete();
		sus();
	}

	public function add(){
		M(CONTROLLER_NAME)->add($data);
		sus();
	}

}