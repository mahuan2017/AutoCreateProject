# Thinkphp3.2.3+layui自动生成后台控制器和视图

#### 介绍
###### 根据您的数据库配置
###### 自动生成出后台所需使用的控制器，方法和视图文件
###### 直接可以进行增删改查
###### 生成完以后，还需要您自己修改修改，然后后台就做完了
###### 不用一直复制粘贴了，真香

#### 软件架构
软件架构说明


#### 安装教程

1.  先去下载thinkphp3.2.3
2.  把我们 AUTO_CREATE_FILE 文件夹放到  thinkphp3.2.3 根目录
3.  下载layui，放在 thinkphp3.2.3 根目录\Public\layui 下面
4.  编辑 AUTO_CREATE_FILE/index.php 数据库配置
5.  编辑 AUTO_CREATE_FILE/index.php 的model数组，写需要生成的表名
6.  运行 AUTO_CREATE_FILE/index.php

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
